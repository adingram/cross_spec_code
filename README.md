# cross_spec_code

## **Setup**

Need to already have fitsio installed.

### Compile:

	gfortran -L/path/to/my/cfitsio/ -lcfitsio -fbounds-check cross_spec.f90 -o cross_spec

e.g. on my desktop: /path/to/my/cfitsio/ = /Data/adamingram/heasoft/heasoft-6.22.1/heacore/cfitsio/
e.g. on my laptop: /path/to/my/cfitsio/ = /usr/local/opt/cfitsio/
The latter will be the path if you install fitsio through Homebrew, which I recommend.

### Run:

	./cross_spec

## **Use**

*Step 1*

You first need to extract ne light curves using mission specific software. Put these light curves
in a directory with a response file (either rmf + arf or just rsp). This response file needs to have
ne channels. You may therefore need to re-bin the response file after creating it using the ftool
rbnrmf. The arf file (if there is one) does not need to be re-binned. The light curve files need to
obey a naming convention:
	root0001.lc
	root0002.lc
	.
	.
	.
	root00ne.lc
where 'root' can be anything you want, and the number must have 4 digits (therefore the maximum allowed
number of light curves is 9999).

*Step 2*

Run the code. It will ask for the following input:



	\> Load file with parameters (true of false)?

Answer true (or t) to load an existing file with parameter values, and false (or f) to enter them to screen.
We will answer false for this example


	\> Enter name of directory containing the light curves and response file(s)

This is the name of the directory (with full path) where the light curves are kept.

10238-01-06-00_lc/


	\> Enter root name of the light curve files

This is the 'root' name from step 1.

counts


	\> Enter name of the response file

pca_xe_1996-03-29.rsp


	\> Enter name of the ancillary response file (or type: none)

none


	\> Enter name of directory where results will be written
	
This directory must already exist
cygx1eg_products/


	\>Enter number of channels (i.e. number of light curves)

This is ne

64


	\> Enter number of time bins per segment (must be integer power of 2)

16384


	\> Enter logarithmic re-binning constant

1.9


	\>Enter minimum channel of reference band

1


	\> Enter maximum channel of reference band
	
64


	\> Enter instrument dead time (seconds)

This is only relevant if you are going to choose to calculate the Poisson noise theoretically

1e-5


	\> Enter number of detectors

This is only relevant if you are going to choose to calculate the Poisson noise theoretically.
For RXTE, this is the number of PCUs

5


	\> Enter fnoisemin (make -ve to use theoretical Poisson noise calc)

If you enter a +ve value, the code will take the average power for f>fnoisemax as the
Poisson noise level. If you enter a -ve value (or a value >the Nyquist frequency of the
light curves), the Poisson noise will be calculated theoretically.

-20.0


	\> Enter filename to write parameters to

These parameters wil be written to a file that you can choose to load at a later date
cygx1eg.txt


*Step 3*

Go to the results directory and type:
	chmod 755 mkpha.xcm
This gives the required permissions to the script mkpha.xcm.

*Step 4*

Run the script:
	./mkpha.xcm
You can now load the pha files into XSPEC


## **Example**

To make a head start, you can run the code on some RXTE data of Cyg X-1 in the hard state:
obsid 10238-01-06-00.


To do this:


	./cross_spec
	
	\>Load file with parameters (true of false)?
	
	t

	\>Enter name of parameter file

	cygx1eg.txt


Then follow steps 3 and 4 from the previous section. You can now load the data into XSPEC as
pha files.

## **Outputs**


All of the code outputs are written to the results directory.


freqbins.dat:

This lists all the frequency bins in the following format

1    	   min frequency     	     max frequency

2    	   min frequency     	     max frequency

.

.

.

nf	   min frequency     	     max frequency,

where nf is the total number of frequency bins (12 for the example)


all_lags.dat:

This plots time lag (s) vs energy (keV) for all frequency bins. Data group 1 is freq bin 1 with Bendat & Piersol (2010) errors,
data group 2 is freq bin 1 with Ingram (2019) errors, data group 3 is freq bin 2 with Bendat & Piersol (2010) errors and so on.


all_cov.dat:

This plots the amplitude of the cross-spectrum vs energy (keV) for all frequency bins (same data group convention as
above). The cross-spectrum is in absolute rms normalization, and so the units of the y-axis are count rate.


After running the pha script, the main output is the pha files:


ReG0001.pha etc:

Real part of the energy-dependent cross-spectrum with Ingram (2019) errors. Fit with reltrans by setting ReIm=1.


ImG0001.pha etc:

Imaginary part of the energy-dependent cross-spectrum with Ingram (2019) errors. Fit with reltrans by setting ReIm=2.


tlag0001.pha etc:

Time lags vs energy with Ingram (2019) errors. Fit with reltrans by setting ReIm=6.


cov0001.pha etc:

Modulus of the energy-dependent cross-spectrum with Ingram (2019) errors. Fit with reltrans by setting ReIm=5.


