program cross_spec
! gfortran -L/Data/adamingram/heasoft/heasoft-6.22.1/heacore/cfitsio/ -lcfitsio cross_spec.f90 -o cross_spec
! gfortran -L/usr/local/opt/cfitsio/ -lcfitsio cross_spec.f90 -o cross_spec
  implicit none
  integer nseg,nn,ne
  real dt,mu,df,f
  real, allocatable :: t(:),C(:),CE(:,:),P(:),dP(:),ref(:)
  character (len=500) infile,indir,root,respfile,resdir,freqbinfile
  integer mm,maxmm,m,k,i,j,minbin,nf,minreal
  integer, allocatable :: segend(:),np(:)
  character (len=4) A
  real, allocatable :: Pbin(:),dPbin(:),far(:)
  real c0,dfbin
  integer refmin,refmax,jmin,nd,Ndet
  real, allocatable :: Prbin(:),dPrbin(:)
  real fnoisemin,man_noise,Psub
  real, allocatable :: ReGE(:,:),dReGE(:,:),ImGE(:,:),dImGE(:,:)
  real, allocatable :: ReGbin(:,:),dReGbin(:,:),ImGbin(:,:),dImGbin(:,:)
  real, allocatable :: sdReGE(:,:),sdImGE(:,:),sdReGbin(:,:),sdImGbin(:,:)
  real, allocatable :: Psbin(:),dPsbin(:),dGbin(:,:),dphi(:,:)
  real gamma2,gammaloop,bias,taud,phi,pi,E,dE,absG,texp
  real, allocatable :: wnr(:),wns(:,:),dabsGbin(:,:),bpdphi(:,:),echn(:)
  integer numchn,nenerg,norm,reunit,imunit,lagunit,ampunit,status,dtype
  character (len=500) refile,imfile,lagfile,ampfile,arffile,phafile,scriptfile
  character (len=500) bprefile,lagdatfile,ampdatfile,bpimfile,parfile
  logical arf
  integer scriptunit,bpreunit,bpimunit
  real, allocatable :: dwnr(:),wnrbin(:),dwnrbin(:),wnsbin(:,:),wn
  integer frequnit,lagdatunit,ampdatunit,parunit
  logical fileload
  pi = acos(-1.0)
  
! ******************** Parameters ********************
  write(*,*)"Load file with parameters (true of false)?"
  read(*,*)fileload

  if( fileload )then
    write(*,*)"Enter name of parameter file"
    read(*,'(a)')parfile
    status = 0
    call ftgiou(parunit,status)
    open(unit=parunit,file=parfile)
    read(parunit,'(a)')indir
    read(parunit,'(a)')root
    read(parunit,'(a)')respfile
    read(parunit,'(a)')arffile
    read(parunit,'(a)')resdir
    read(parunit,*)ne
    read(parunit,*)nseg
    read(parunit,*)c0
    read(parunit,*)refmin
    read(parunit,*)refmax
    read(parunit,*)taud
    read(parunit,*)Ndet
    read(parunit,*)fnoisemin
    status = 0
    call ftclos(parunit,status)
    call ftfiou(parunit,status)
  else
    !Read parameters from terminal
    write(*,*)"Enter name of directory containing the light curves and response file(s)"
    read(*,'(a)')indir
    write(*,*)"Enter root name of the light curve files"
    read(*,'(a)')root
    write(*,*)"Enter name of the response file"
    read(*,'(a)')respfile
    write(*,*)"Enter name of the ancillary response file (or type: none)"
    read(*,'(a)')arffile
    write(*,*)"Enter name of directory where results will be written"
    read(*,'(a)')resdir
    write(*,*)"Enter number of channels (i.e. number of light curves)"
    read(*,*)ne
    write(*,*)"Enter number of time bins per segment (must be integer power of 2)"
    read(*,*)nseg
    write(*,*)"Enter logarithmic re-binning constant"
    read(*,*)c0
    write(*,*)"Enter minimum channel of reference band"
    read(*,*)refmin
    write(*,*)"Enter maximum channel of reference band"
    read(*,*)refmax
    write(*,*)"Enter instrument dead time (seconds)"
    read(*,*)taud
    write(*,*)"Enter number of detectors"
    read(*,*)Ndet
    write(*,*)"Enter fnoisemin (make -ve to use theoretical Poisson noise calc)"
    read(*,*)fnoisemin
    write(*,*)"Enter filename to write parameters to"
    read(*,'(a)')parfile
    !Write parameters to file
    status = 0
    call ftgiou(parunit,status)
    open(unit=parunit,file=parfile)
    write(parunit,'(a)')trim(indir)
    write(parunit,'(a)')trim(root)
    write(parunit,'(a)')trim(respfile)
    write(parunit,'(a)')trim(arffile)
    write(parunit,'(a)')trim(resdir)
    write(parunit,*)ne
    write(parunit,*)nseg
    write(parunit,*)c0
    write(parunit,*)refmin
    write(parunit,*)refmax
    write(parunit,*)taud
    write(parunit,*)Ndet
    write(parunit,*)fnoisemin
    status = 0
    call ftclos(parunit,status)
    call ftfiou(parunit,status)
  end if

  ! Example parameters
  ! indir   = '/Data/adamingram/Dropbox/Reverberation/cygx-1/P10238/10238-01-06-00/lc' 
  ! root    = 'counts'
  ! respfile = 'pca_xe_1996-03-29.rsp'  !Name of response file (must be in indir)
  ! arffile  = 'none'
  ! resdir   = 'products/'              !Directory where results will be kept
  ! ne      = 64
  ! nseg    = 2**14
  ! c0      = 1.9
  ! refmin  = 1       !minimum channel number in reference band
  ! refmax  = 64       !maximum channel number in reference band
  ! taud    = 1e-5    !deadtime in seconds
  ! Ndet    = 5       !number of PCUs switched on  
  ! fnoisemin = -20.0   !frequency above which we assume is dominated by Poisson noise
  !                    !(make -ve to use theoretical value)
  
! ******************** Setup ********************

  minreal = 40      !minimum number of realizations
  
  !norm = 1: fractional rms normalization
  !norm = 2: absolute rms normalization
  norm = 2

  arf = .true.
  if( trim(arffile) .eq. 'none' .or. trim(arffile) .eq. 'NONE' ) arf = .false.
  
! Get energy array from response file
  respfile = trim(indir) // '/' // trim(respfile)
  call getEsize(respfile,numchn,nenerg)
  allocate( echn(0:numchn) )
  call getEbounds(respfile,numchn,echn)

! Name light curve to use for setup
  infile = trim(indir) // '/' // trim(root) // '0001.lc'
  
! Read the number of time bins and the duration of each bin
  call getnnanddt(infile,nn,dt)
  write(*,*)"Number of time bins in light curve=",nn
  write(*,*)"Time step of light curve (s)=",dt
  
! Allocate arrays
  allocate( t(nn) )
  allocate( C(nn) )
  allocate( CE(ne,nn) )
  allocate( ref(nn) )
  maxmm = ceiling( real(nn) / real(nseg) )
  allocate( segend(maxmm) )
  allocate(  P(nseg/2) )
  allocate( dP(nseg/2) )

! Sort the segments
  call segsort(infile,nn,dt,nseg,maxmm,mm,segend,t)
  write(*,*)"Number of segments in light curve=",mm
  texp = nseg*dt*mm
  write(*,*)"Useful exposure (s)=",texp
  
! Define frequency grids
  df = 1.0 / ( real(nseg) * dt )
  minbin = ceiling( real(minreal) / real(mm) )
  call getnf(nseg,c0,minbin,nf)
  allocate( far(0:nf) )
  allocate( np(nf)    )
  allocate( Pbin(nf)  )
  allocate( dPbin(nf) )
  allocate( Prbin(nf)  )
  allocate( dPrbin(nf) )
  call binscheme(nseg,df,c0,minbin,nf,far,np)

! Write out frequency binning scheme
  status = 0
  call ftgiou(frequnit,status)
  freqbinfile = trim(resdir) // '/freqbins.dat' 
  open(frequnit,file=freqbinfile)
  do j = 1,nf
    write(frequnit,*)j,far(j-1),far(j)
  end do
  close(frequnit)
  call ftfiou(frequnit, status)
  
! ******************** Read light curves ********************

  write(*,*)"Reading in light curves..."
  do i = 1,ne
    !Name light curve
    call mkfourchar(i,A)
    infile = trim(indir) // '/' // trim(root) // A(1:4) // '.lc'
    write(*,*)trim(infile)
    !Read in light curve
    call readlc(infile,nn,dt,CE(i,:))    
  end do
  write(*,*)"...Finished reading in light curves"

  !Create reference band
  ref = 0.0
  do i = refmin,refmax
     do j = 1,nn
        ref(j) = ref(j) + CE(i,j)
     end do
  end do
  
! ******************** Calculate cross-spectra ********************

  ! Allocate all remaining arrays
  allocate(  ReGE(ne,nseg/2) )
  allocate( dReGE(ne,nseg/2) )
  allocate( sdReGE(ne,nseg/2) )
  allocate(  ImGE(ne,nseg/2) )
  allocate( dImGE(ne,nseg/2) )
  allocate( sdImGE(ne,nseg/2) )
  allocate(  ReGbin(ne,nf) )
  allocate( dReGbin(ne,nf) )
  allocate( sdReGbin(ne,nf) )
  allocate(  ImGbin(ne,nf) )
  allocate( dImGbin(ne,nf) )
  allocate( sdImGbin(ne,nf) )
  allocate(  Psbin(nf) )
  allocate( dPsbin(nf) )
  allocate( dGbin(ne,nf) )
  allocate( wnr(nseg/2)    )
  allocate( wns(ne,nseg/2) )
  allocate( dphi(ne,nf) )
  allocate( dabsGbin(ne,nf) )
  allocate( bpdphi(ne,nf) )
  allocate( dwnr(nseg/2) )
  allocate( wnrbin(nf) )
  allocate( dwnrbin(nf) )
  allocate( wnsbin(ne,nf) )
  
  !Calculate and bin the reference band power spectrum
  call adv_mypow(nseg,ref,nn,dt,mm,segend,norm,Ndet,taud,P,dP,mu,wnr)
  dwnr = 0.0
  call binup(nseg,nf,np,P,dP,Prbin,dPrbin)
  call binup(nseg,nf,np,wnr,dwnr,wnrbin,dwnrbin)
  !Calculate Poisson noise manually
  if( fnoisemin .gt. 0.0 .and. fnoisemin .lt. (0.5/dt) )then
     wn = man_noise(nseg,P,df,fnoisemin)
     do j = 1,nf
       wnrbin(j) = wn
     end do
  end if
  
  !Calculate the cross-spectrum with error bars
  write(*,*)"Calculating cross-spectra..."
  do i = 1,ne
    !Calculate and bin power spectrum
    call adv_mypow(nseg,CE(i,:),nn,dt,mm,segend,norm,Ndet,taud,P,dP,mu,wns(i,:))
    call binup(nseg,nf,np,P,dP,Psbin,dPsbin)
    call binup(nseg,nf,np,wns(i,:),dwnr,wnsbin(i,:),dwnrbin)
    !Calculate and bin cross-spectrum
    call newcrossf(nseg,CE(i,:),ref,nn,dt,mm,segend,norm,ReGE(i,:),sdReGE(i,:),ImGE(i,:),sdImGE(i,:))
    call binup(nseg,nf,np,ReGE(i,:),sdReGE(i,:),ReGbin(i,:),sdReGbin(i,:))
    call binup(nseg,nf,np,ImGE(i,:),sdImGE(i,:),ImGbin(i,:),sdImGbin(i,:))
    !Calculate Poisson noise manually
    if( fnoisemin .gt. 0.0 .and. fnoisemin .lt. (0.5/dt) )then
       wn = man_noise(nseg,P,df,fnoisemin)
       do j = 1,nf
         wnsbin(i,j) = wn
       end do
    end if
    !Subtract Poisson noise contribution
    if( i .ge. refmin .and. i .le. refmax ) ReGbin(i,:) = ReGbin(i,:) - wnsbin(i,:)
    !Calculate errors
    do j = 1,nf
       f     = 0.5 * ( far(j) + far(j-1) )
       dfbin = 0.5 * ( far(j) - far(j-1) )
       nd = mm*np(j)
       gamma2 = gammaloop(Prbin(j),wnrbin(j),Psbin(j),wnsbin(i,j),ReGbin(i,j),ImGbin(i,j),nd,bias)
       !B&P
       call bp_err(ReGbin(i,j),ImGbin(i,j),Prbin(j),Psbin(j),bias,nd,dReGbin(i,j),dImGbin(i,j),dabsGbin(i,j),bpdphi(i,j))
       !My errors
       call ingram_err(ReGbin(i,j),ImGbin(i,j),Prbin(j),wnrbin(j),Psbin(j),bias,nd,dGbin(i,j),dphi(i,j))   
    end do
  end do
  write(*,*)"...Finished calculating cross-spectra"
  
! ******************** Write out cross-spectra ********************

  status = 0
  call ftgiou(lagdatunit,status)
  lagdatfile = trim(resdir) // '/all_lags.dat'
  open(lagdatunit,file=lagdatfile)
  write(lagdatunit,*)"read serr 1 2 3"
  write(lagdatunit,*)"skip on"
  status = 0
  call ftgiou(ampdatunit,status)
  ampdatfile = trim(resdir) // '/all_cov.dat'
  open(ampdatunit,file=ampdatfile)
  write(ampdatunit,*)"read serr 1 2 3"
  write(ampdatunit,*)"skip on"
  
  scriptfile = trim(resdir) // '/mkpha.xcm'
  status = 0
  call ftgiou(scriptunit,status)
  open(scriptunit,file=scriptfile)
  dtype = 2
  
  do j = 1,nf
     f     = 0.5 * ( far(j) + far(j-1) )
     call mkfourchar(j,A)
     !Name the output files
     refile  = 'ReG' // A(1:4) // '.dat'
     imfile  = 'ImG' // A(1:4) // '.dat'
     lagfile = 'tlag' // A(1:4) // '.dat'
     ampfile = 'cov' // A(1:4) // '.dat'
     !Write scripts that will convert ascii files to pha
     phafile = 'ReG' // A(1:4) // '.pha'
     call mkscript(scriptunit,refile,phafile,texp,numchn,respfile,arffile,arf,dtype)
     phafile = 'ImG' // A(1:4) // '.pha'
     call mkscript(scriptunit,imfile,phafile,texp,numchn,respfile,arffile,arf,dtype)
     call flxscript(scriptunit,lagfile)
     call flxscript(scriptunit,ampfile)     
     !Place output files in the results directory
     refile = trim(resdir) // '/' // trim(refile)
     imfile = trim(resdir) // '/' // trim(imfile)
     lagfile = trim(resdir) // '/' // trim(lagfile)
     ampfile = trim(resdir) // '/' // trim(ampfile)
     !Select unused unit number
     status = 0
     call ftgiou(reunit,status)
     call ftgiou(imunit,status)
     call ftgiou(lagunit,status)
     call ftgiou(ampunit,status)
     !Open output files
     open(reunit,file=refile)
     open(imunit,file=imfile)
     open(lagunit,file=lagfile)
     open(ampunit,file=ampfile)
     !
     !Also write out real and imaginary parts with the BP errors
     !
     bprefile  = 'BPReG' // A(1:4) // '.dat'
     phafile = 'BPReG' // A(1:4) // '.pha'
     call mkscript(scriptunit,bprefile,phafile,texp,numchn,respfile,arffile,arf,dtype)
     bprefile = trim(resdir) // '/' // trim(bprefile)
     status = 0
     call ftgiou(bpreunit,status)
     open(bpreunit,file=bprefile)
     bpimfile  = 'BPImG' // A(1:4) // '.dat'
     phafile = 'BPImG' // A(1:4) // '.pha'
     call mkscript(scriptunit,bpimfile,phafile,texp,numchn,respfile,arffile,arf,dtype)
     bpimfile = trim(resdir) // '/' // trim(bpimfile)
     status = 0
     call ftgiou(bpimunit,status)
     open(bpimunit,file=bpimfile)
     !
     do i = 1,ne
        !Calculate outputs
        E     = 0.5 * ( echn(i) + echn(i-1) )
        dE    = 0.5 * ( echn(i) - echn(i-1) )
        phi = atan2( ImGbin(i,j) , ReGbin(i,j) )
        absG = sqrt( ReGbin(i,j)**2 + ImGbin(i,j)**2 )
        !Write out illustrative plots
        write(lagdatunit,*)E,dE,phi/(2.0*pi*f),bpdphi(i,j)/(2.0*pi*f),phi/(2.0*pi*f),dphi(i,j)/(2.0*pi*f)
        write(ampdatunit,*)E,dE,absG,dabsGbin(i,j),absG,dGbin(i,j)
        !Write out xspec outputs
        write(reunit,*)i-1,ReGbin(i,j),dGbin(i,j)
        write(bpreunit,*)i-1,ReGbin(i,j),dReGbin(i,j)
        write(imunit,*)i-1,ImGbin(i,j),dGbin(i,j)
        write(bpimunit,*)i-1,ImGbin(i,j),dImGbin(i,j)
        write(lagunit,*)echn(i-1),echn(i),2.0*dE*phi/(2.0*pi*f),2.0*dE*dphi(i,j)/(2.0*pi*f)
        write(ampunit,*)echn(i-1),echn(i),2.0*dE*absG,2.0*dE*dGbin(i,j)
     end do
     write(lagdatunit,*)"no no"
     write(ampdatunit,*)"no no"
     !Close and free up units
     status = 0
     close(reunit)
     call ftfiou(reunit, status)
     close(imunit)
     call ftfiou(imunit, status)
     close(lagunit)
     call ftfiou(lagunit, status)
     close(ampunit)
     call ftfiou(ampunit, status)
     close(bpreunit)
     call ftfiou(bpreunit,status)
     close(bpimunit)
     call ftfiou(bpimunit,status)
  end do

  status = 0
  close(scriptunit)
  call ftfiou(scriptunit, status)

  write(lagdatunit,*)"la y Time Lag (s)"
  write(lagdatunit,*)"la x Energy (keV)"
  write(lagdatunit,*)"log x"
  write(ampdatunit,*)"la y |G(n)|"
  write(ampdatunit,*)"la x Energy (keV)"
  write(ampdatunit,*)"log"

  status = 0
  close(lagdatunit)
  call ftfiou(lagdatunit,status)
  close(ampdatunit)
  call ftfiou(ampdatunit,status)

  write(*,*)"----------------------------------------------------------"
  write(*,*)"Outputs in ",trim(resdir),":"
  write(*,*)"freqbins.dat: lists frequency ranges used"
  write(*,*)"all_lags.dat: time lags with BP and I errors"
  write(*,*)"all_cov.dat: |G(n)| with BP and I errors"
  write(*,*)"mkpha.xcm: chmod 755 this and run. Will create pha files"
  write(*,*)"pha files: ReG,ImG,cov,tlag,BPReG,BImG ...see readme"
  write(*,*)"... BP = Bendat & Piersol (2010), I = Ingram (2019)"
  write(*,*)"----------------------------------------------------------"
  
end program cross_spec



!-----------------------------------------------------------------------
subroutine flxscript(unit,infile)
!Writes out a command for flx2xsp when given the name of the input ascii file
  implicit none
  character (LEN=500) infile,com
  integer unit,len
  len = len_trim(infile)
  com = 'flx2xsp ' // trim(infile)
  com = trim(com) // ' ' // infile(1:len-4) // '.pha'
  com = trim(com) // ' ' // infile(1:len-4) // '.rsp'
  write(unit,*)trim(com)
  RETURN
END subroutine flxscript
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
      subroutine mkscript(unit,infile,outfile,texp,numchn,respname,arfname,arf,dtype)
! Writes acsii2pha script to convert a data file to a fits file
      implicit none
      character (len=1000) com
      character (len=4) chantype,ebins
      character (len=1) Adtype
      character (len=500) infile,outfile,respname,arfname
      real texp
      integer numchn,status,unit,dtype
      logical arf
      chantype = 'PHA'
      call mkfourchar(numchn,ebins)
      call mkonechar(dtype,Adtype)      
      com = 'ascii2pha infile=' // trim(infile)
      com = trim(com) // ' outfile=' // trim(outfile)
      com = trim(com) // ' chanpres=yes dtype=' // Adtype
      com = trim(com) // ' qerror=yes rows=-'
      com = trim(com) // ' tlmin=0'
      com = trim(com) // ' detchans=' // trim(ebins)
      com = trim(com) // ' telescope=' // 'xray'
      com = trim(com) // ' instrume=' // 'xray'
      com = trim(com) // ' detnam=' // 'xray'
      com = trim(com) // ' filter=NONE'
      com = trim(com) // ' respfile=' // trim(respname)
      if( arf ) com = trim(com) // ' ancrfile=' // trim(arfname)
      com = trim(com) // ' backfile=none'
      com = trim(com) // ' chantype=' // trim(chantype)
      com = trim(com) // ' exposure='
      write(unit,*)trim(com),texp
      return
      end subroutine mkscript
!-----------------------------------------------------------------------
      

    



!-----------------------------------------------------------------------
subroutine ingram_err(ReG,ImG,Pr,wnr,Ps,bias,nd,dG,dphi)
! Calculates Ingram (2019) errors on cross-spectrum and phase lag
! Inputs:
! ReG, ImG      Real and imaginary parts of the cross-spectrum
! Pr            Reference band power spectrum (*includes* Poisson noise)
! wnr           Poisson noise level of reference band
! Ps            Subject band power spectrum (*includes* Poisson noise)
! bias          Bias (calculate using the gammaloop function)
! nd            Number of realisations used for average  
! Outputs:
! dG            Error on real part, imaginary part, and modulus of cross-spectrum
! dphi          Error on phase of cross-spectrum (in radians)
  implicit none
  integer nd
  real ReG,ImG,Pr,wnr,Ps,bias,dG,dphi,absG2
  absG2 = ReG**2 + ImG**2 - bias
  dG    = Ps - absG2 / ( Pr - wnr )
  dG    = dG * Pr / real(2*nd)
  dphi  = dG / absG2
  dG    = sqrt( dG )
  dphi  = sqrt( dphi )
  return
end subroutine ingram_err
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
subroutine bp_err(ReG,ImG,Pr,Ps,bias,nd,dReG,dImG,dabsG,dphi)
! Calculates Bendat & Piersol (2010) errors on cross-spectrum and phase lag
! Inputs:
! ReG, ImG      Real and imaginary parts of the cross-spectrum
! Pr            Reference band power spectrum (*includes* Poisson noise)
! Ps            Subject band power spectrum (*includes* Poisson noise)
! bias          Bias (calculate using the gammaloop function)
! nd            Number of realisations used for average  
! Outputs:
! dReG          Error on real part of the cross-spectrum
! dImG          Error on imaginary part of the cross-spectrum
! dabsG         Error on modulus of the cross-spectrum
! dphi          Error on phase of cross-spectrum (in radians)
  implicit none
  integer nd
  real ReG,ImG,Pr,Ps,bias,dReG,dImG,dabsG,dphi,g2
  dReG  = Pr * Ps + ReG**2 - ImG**2
  dReG  = sqrt( dReG / real(2*nd) )
  dImG  = Pr * Ps - ReG**2 + ImG**2
  dImG  = sqrt( dImG / real(2*nd) )
  dabsG = sqrt( Pr * Ps / real(nd) )
  g2    = ( ReG**2 + ImG**2 - bias ) / ( Pr * Ps )
  dphi  = (1.0-g2) / g2
  dphi  = sqrt( dphi / real(2*nd) )
  return
end subroutine bp_err  
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
function gammaloop(Pr,wnr,Ps,wns,ReG,ImG,nd,b2)
  implicit none
  integer it,nit,nd
  parameter (nit=100)
  real Pr,wnr,Ps,wns,ReG,ImG,gammaloop,gamma2,b2,xacc,prev
  xacc = 1e-5
  prev   = 0.0
  gamma2 = 1.0
  b2     = 0.0
  it = 0
  do while( abs(gamma2-prev) .gt. xacc .and. it .lt. nit )
    it   = it + 1
    prev = gamma2
    gamma2 = ( ReG**2 + ImG**2 - b2 ) / ( ( Pr - wnr ) * ( Ps - wns ) )
!    gamma2 = min( gamma2 , 1.0 )
!    gamma2 = max( gamma2 , 0.0 )
    b2     = ( Pr * Ps - gamma2 * ( Pr - wnr ) * ( Ps - wns ) ) / real(nd)
!    write(*,*)it,gamma2,b2
  end do
  gammaloop = gamma2
  return
end function gammaloop
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
subroutine binup(nseg,nf,np,P,dP,Pbin,dPbin)
  !Input:
  !nseg          Number of raw frequency bins
  !nf            Number of new bins
  !np(nf)        Number of raw frequency bins per new bin
  !P(nseg/2)     Raw power
  !dP(nseg/2)    Error on raw power
  !Output
  !Pbin(nf)      Binned power
  !dPbin(nf)     Error on binned power
  implicit none
  integer nseg,nf,np(nf)
  real P(nseg/2),dP(nseg/2),Pbin(nf),dPbin(nf)
  integer imin,imax,i,j
  imin = 1
  do j = 1,nf
    imax = imin + np(j) - 1
    Pbin(j)  = 0.0
    dPbin(j) = 0.0
    do i = imin,imax
      Pbin(j)  =  Pbin(j) + P(i)
      dPbin(j) = dPbin(j) + dP(i)**2
    end do
    Pbin(j)  = Pbin(j) / real( np(j) )
    dPbin(j) = sqrt( dPbin(j) ) / real( np(j) )
    imin = imax + 1
  end do
  return
end subroutine binup    
!-----------------------------------------------------------------------


   
!-----------------------------------------------------------------------
subroutine newcrossf(nseg,C,ref,nnmax,dt,mm,segend,norm,ReG,sdReG,ImG,sdImG)
! c INPUT:
! c nseg          Number of time bins in a segment
! c C(nnmax)      Subject band time series to transform
! c ref(nnmax)    Reference band time series to transform
! c nnmax         Maximum length of time series
! c dt            Time interval
! c mm            Number of segments
! c segend(mm)    Last time bin in each segment
! c norm          norm=1: fractional rms; norm=2: absolute rms
! c OUTPUT:
! c ReG(nseg/2)   Real part of cross-spectrum
! c sdReG(nseg/2) Standard deviation of real part of cross-spectrum
! c ImG(nseg/2)   Imaginary part of cross-spectrum
! c sdImG(nseg/2) Standard deviation of imaginary part of cross-spectrum
  implicit none
  integer nseg,nnmax,mm,segend(nnmax/nseg),norm
  real C(nnmax),ref(nnmax),dt,ReG(nseg/2),sdReG(nseg/2)
  real ImG(nseg/2),sdImG(nseg/2)
  integer m,i,j
  real Cseg(nseg),refseg(nseg)
  real rc(mm,nseg/2),ic(mm,nseg/2),mu,muref
  ReG   = 0.0
  sdReG = 0.0
  ImG   = 0.0
  sdImG = 0.0
  mu    = 0.0
  muref = 0.0
  !Calculate cross-spectrum in absolute rms normalisation
  do m = 1,mm
    do i = 1,nseg
      j = i + segend(m) - nseg
      Cseg(i)   = C(j)
      refseg(i) = ref(j)
      mu        = mu + C(j)
      muref     = muref + ref(j)
    end do
    call ncperiodogram(dt,nseg,Cseg,refseg,rc(m,:),ic(m,:))
    do i = 1,nseg/2
       ReG(i) = ReG(i) + rc(m,i)
       ImG(i) = ImG(i) + ic(m,i)
    end do
  end do
  mu    = mu      / real( nseg*mm )
  muref = muref   / real( nseg*mm )
  ReG   = ReG     / real( mm )
  ImG   = ImG     / real( mm )
  !Calculate standard deviation
  do m = 1,mm
     do i = 1,nseg/2
        sdReG(i) = sdReG(i) + ( ReG(i) - rc(m,i) )**2
        sdImG(i) = sdImG(i) + ( ImG(i) - ic(m,i) )**2
     end do
  end do
  sdReG = sqrt(sdReG) / real(mm)
  sdImG = sqrt(sdImG) / real(mm)
  !Convert to fractional rms normalisation
  if( abs(norm) .eq. 1 )then
    ReG   = ReG     / ( mu * muref )
    ImG   = ImG     / ( mu * muref )
    sdReG   = sdReG     / ( mu * muref )
    sdImG   = sdImG     / ( mu * muref )
  end if
  RETURN
END subroutine newcrossf
!-----------------------------------------------------------------------






!-----------------------------------------------------------------------
function man_noise(nseg,P,df,fnoisemin)
! Calculates whit noise level as average power above fnoisemin
! Input:
! nseg          Number of frequency bins = nseg/2
! P(nseg/2)     Power spectrum (includes white noise)
! df            Frequency resolution
! fnoisemin     Minimum frequency considered to be dominated by white noise
! Output:
! man_noise     Estimated white noise level
  implicit none
  integer nseg
  real man_noise,P(nseg/2),df,fnoisemin
  integer jmin,j
  real wnav
  jmin = ceiling( fnoisemin / df )
  wnav = 0.0
  do j = jmin,nseg/2
     wnav = wnav + P(j)
  end do
  wnav = wnav / real( nseg/2 - jmin + 1 )
  man_noise = wnav
  return
end function man_noise
!-----------------------------------------------------------------------



    
!-----------------------------------------------------------------------
      subroutine mycrossf(nseg,Cj,Cn,nnmax,dt,mm,segend,norm,Cross,sig)
! c INPUT:
! c nseg          Number of time bins in a segment
! c Cj(nnmax)     One time series to transform
! c Cn(nnmax)     The other time series to transform
! c nnmax         Maximum length of time series
! c dt            Time interval
! c mm            Number of segments
! c segend(mm)    Last time bin in each segment
! c norm          norm=1: fractional rms; norm=2: absolute rms
! c OUTPUT:
! c Cross(nseg/2) Complex cross-spectrum coefficient (+ve phase means j lags n)
! c sig(nseg/2)   Error on the cross-spectrum
      implicit none
      integer nseg,nnmax,mm,segend(nnmax/nseg),norm
      real Cj(nnmax),Cn(nnmax),dt,sig(nseg/2)
      complex Cross(nseg/2)
      integer m,i,j
      real Pj(nseg/2),Pn(nseg/2),Cjseg(nseg),Cnseg(nseg)
      real rc(nseg/2),ic(nseg/2),muj,mun
      Cross = 0.0
      Pj    = 0.0
      Pn    = 0.0
      sig   = 0.0
      muj   = 0.0
      mun   = 0.0
      !Calculate cross-spectrum in absolute rms normalisation
      do m = 1,mm
        do i = 1,nseg
          j = i + segend(m) - nseg
          Cjseg(i) = Cj(j)
          Cnseg(i) = Cn(j)
          muj      = muj + Cj(j)
          mun      = mun + Cn(j)
        end do
        call ncperiodogram(dt,nseg,Cjseg,Cnseg,rc,ic)
        do i = 1,nseg/2
          Cross(i) = Cross(i) + complex( rc(i) , ic(i) )
        end do
        call ncperiodogram(dt,nseg,Cjseg,Cjseg,rc,ic)
        Pj = Pj + rc
        call ncperiodogram(dt,nseg,Cnseg,Cnseg,rc,ic)
        Pn = Pn + rc
      end do
      muj   = muj   / float( nseg*mm )
      mun   = mun   / float( nseg*mm )
      Cross = Cross / float( mm )
      Pj    = Pj    / float( mm )
      Pn    = Pn    / float( mm )
      sig   = sqrt( Pn * Pj / ( 2.0 * float(mm) ) )
      !Convert to fractional rms normalisation
      if( abs(norm) .eq. 1 )then
        Cross = Cross / ( muj * mun )
        sig   = sig   / ( muj * mun )
      end if
      RETURN
    END subroutine mycrossf
!-----------------------------------------------------------------------




!-----------------------------------------------------------------------
subroutine getnf(nseg,c0,minbin,nf)
  !Input:
  !nseg          Number of raw frequency bins
  !c0            Geometric re-binning constant
  !minbin        Minimum number of raw bins in a new bin
  !Output:
  !nf            Number of new bins
  implicit none
  integer nseg,minbin,nf
  real c0
  integer i,j,remain,np
  i = 0
  j = 0
  do while( i .lt. nseg/2 )
    j      = j + 1
    remain = nseg/2 - i
    np     = max( minbin , floor( c0**j ) )
    np     = min( np , remain )
    i      = i + np
  end do
  nf      = j - 1
  if( np .lt. minbin ) nf = nf - 1
  return
end subroutine getnf
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
subroutine binscheme(nseg,df,c0,minbin,nf,far,np)
  !Input:
  !nseg          Number of raw frequency bins
  !df            Raw frequency resolution
  !c0            Geometric re-binning constant
  !minbin        Minimum number of raw bins in a new bin
  !nf            Number of new bins
  !Output
  !far(0:nf)     Bounds of new frequency bins
  !np(nf)        Number of raw frequency bins per new bin
  implicit none
  integer nseg,minbin,nf,np(nf)
  real df,c0,far(0:nf)
  integer i,j,remain
  far(0) = 0.5 * df
  i = 0
  j = 1
  do while( j .lt. nf )
    remain = nseg/2 - i
    np(j)  = max( minbin , floor( c0**j ) )
    np(j)  = min( np(j) , remain )
    i      = i + np(j)
    far(j) = ( i + 0.5 ) * df
    j      = j + 1
  end do
  remain  = nseg/2 - i
  np(nf)  = remain
  far(nf) = ( nseg/2 + 0.5 ) * df
  return
end subroutine binscheme
!-----------------------------------------------------------------------

    

!-----------------------------------------------------------------------
subroutine getEsize(infile,numchn,nenerg)
! Opens response file and determines the dimensions of the response matrix
! Inputs:
! infile        Name of response file (including full path)
! Outputs:
! numchn        Number of energy channels
! nenerg        Size of telescope internal energy grid
  implicit none
  character (len=500) infile
  integer numchn,nenerg
  integer status,readwrite,unit,blocksize,n_ebounds,delta_n,hdutype
  character (len=500) comment
  !-----------------------------------------------------------------
  !Find a unit number not currently being used
  status = 0
  call ftgiou(unit,status)
  if( status .ne. 0 ) stop 'cannot open unused unit number'
  !-----------------------------------------------------------------
  !Open the response fits file with read-only access
  readwrite = 0
  status    = 0
  call ftopen(unit,infile,readwrite,blocksize,status)
  if( status .ne. 0 ) stop 'cannot open response file'
  !-----------------------------------------------------------------
  !Shift to extension "EBOUNDS"
  status = 0
  call FTMNHD(unit, 2, 'EBOUNDS', 0, status)
  if(status .ne. 0) stop 'Cannot shift to EBOUNDS extension'
  !-----------------------------------------------------------------
  !Get numchn
  call ftgkyj(unit,'NAXIS2',numchn,comment,status)
  if (status .ne. 0) stop 'Cannot determine NUMCHN'
  !-----------------------------------------------------------------
  !Get the number of the current HDU
  call FTGHDN(unit,n_ebounds)
  !write(*,*)"n_ebounds=",n_ebounds
  !-----------------------------------------------------------------
  !Use to work out the HDU number of the matrix extension
  if( n_ebounds .eq. 2 )then
     delta_n = 1
  else
     delta_n = -1
  end if
  !-----------------------------------------------------------------
  !Shift to matrix extension
  status = 0
  call FTMRHD(unit,delta_n,hdutype,status)
  if(status .ne. 0) stop 'Cannot shift to MATRIX extension'
  !-----------------------------------------------------------------
  !Get nenerg
  call ftgkyj(unit,'NAXIS2',nenerg,comment,status)
  if (status .ne. 0) stop 'Cannot determine nenerg'
  !-----------------------------------------------------------------
  !Close and free up unit
  call ftclos(unit,status)
  call ftfiou(unit, status)
  return
end subroutine getEsize  
!-----------------------------------------------------------------------




!-----------------------------------------------------------------------
subroutine getEbounds(infile,numchn,echn)
! Opens response file and determines the dimensions of the response matrix
! Inputs:
! infile           Name of response file (including full path)
! numchn           Number of energy channels
! Outputs:
! echn(0:numchn)   Energy array for channels
  implicit none
  character (len=500) infile
  integer numchn
  real echn(0:numchn),nullval
  integer status,readwrite,unit,blocksize,n_ebounds,delta_n,hdutype
  character (len=500) comment
  integer i,colnum,felem,nelem
  logical anynull
  !-----------------------------------------------------------------
  !Find a unit number not currently being used
  status = 0
  call ftgiou(unit,status)
  if( status .ne. 0 ) stop 'cannot open unused unit number'
  !-----------------------------------------------------------------
  !Open the response fits file with read-only access
  readwrite = 0
  status    = 0
  call ftopen(unit,infile,readwrite,blocksize,status)
  if( status .ne. 0 ) stop 'cannot open response file'
  !-----------------------------------------------------------------
  !Shift to extension "EBOUNDS"
  status = 0
  call FTMNHD(unit, 2, 'EBOUNDS', 0, status)
  if(status .ne. 0) stop 'Cannot shift to EBOUNDS extension'
  !-----------------------------------------------------------------
  !Read in the ranges of the energy channels
  do I = 1,NUMCHN
    !---------------------------------------------------------------
    !Read in E_MIN
    colnum  = 2
    felem   = 1
    nelem   = 1
    nullval = -1.0
    anynull = .false.
    status  = 0
    call ftgcve(unit,colnum,I,felem,nelem,nullval,ECHN(I-1),anynull,status)
    !---------------------------------------------------------------
    !Read in E_MAX
    colnum  = 3
    call ftgcve(unit,colnum,I,felem,nelem,nullval,ECHN(I),anynull,status)
    if( status .ne. 0 ) stop 'problem reading in EBOUNDS'
  end do
  !-----------------------------------------------------------------
  !Close and free up unit
  call ftclos(unit,status)
  call ftfiou(unit, status)
  return
end subroutine getEbounds
!-----------------------------------------------------------------------




!-----------------------------------------------------------------------
subroutine readlc(infile,nn,dt,C)
  !Reads lght curve from fits file
  !Inputs:
  !infile         Name of fits file containing light curve (char 500)
  !nn             Number of time bins in light curve (real)
  !dt             Duration of each time bin (integer)
  !Outputs:
  !C(nn)          Count rate in each time bin
  implicit none
  character (len=500) infile
  integer nn
  real dt,C(nn)
  integer status,unit,readwrite,blocksize,nn1
  character (len=500) comment
  real dt1
  logical exact,anynull
  integer hdutype,k,rcol
  double precision RATE
  !-----------------------------------------------------------------
  !Find a unit number not currently being used
  status = 0
  call ftgiou(unit,status)
  if( status .ne. 0 ) stop 'cannot open unused unit number'
  !-----------------------------------------------------------------
  !Open the light curve fits file with read-only access
  readwrite = 0
  status    = 0
  !write(*,*)trim(infile)
  call ftopen(unit,infile,readwrite,blocksize,status)
  if( status .ne. 0 ) stop 'cannot open light curve file'
  !-----------------------------------------------------------------
  !Shift to extension "RATE"
  status = 0
  call FTMNHD(unit, 2, 'RATE', 0, status)
  if(status .ne. 0) stop 'Cannot shift to RATE extension'
  !-----------------------------------------------------------------
  !Check number of rows in the table: nn1
  status = 0
  call ftgkyj(unit,'NAXIS2',nn1,comment,status)
  if(status .ne. 0) stop 'Cannot determine nn'
  if(nn.ne.nn1) stop 'nn changed!!!!'
  !-----------------------------------------------------------------
  !Check the time interval: dt1
  status = 0
  call ftgkye(unit,'TIMEDEL',dt1,comment,status)
  if(status .ne. 0) stop 'Cannot determine dt'
  if( abs(dt-dt1) .gt. 1e-5 ) stop 'dt changed!!!!'
  !-----------------------------------------------------------------
  !Get the column number for rate
  exact=.false.
  call ftgcno(unit,exact,'RATE',rcol,status)
  !-----------------------------------------------------------------
  !Read in rate
  do k = 1,nn
    call ftgcvd(unit,rcol,k,1,1,-1.0,RATE,anynull,status)
    C(k) = REAL( RATE )
  end do
  if( anynull ) write(*,*)'Problem reading in light curve'
  !Close and free up unit
  call ftclos(unit,status)
  call ftfiou(unit, status)
  return
end subroutine readlc
!-----------------------------------------------------------------------











!-----------------------------------------------------------------------
subroutine segsort(infile,nn,dt,nseg,maxmm,mm,segend,t)
  !Sorts the light curve into segments nseg bins long
  !Inputs:
  !infile         Name of fits file containing light curve (char 500)
  !nn             Number of time bins in light curve (real)
  !dt             Duration of each time bin (integer)
  !nseg           Number of time bins in a segment
  !maxmm          Maximum possible number of segments
  !Outputs:
  !mm             Number of segments in the light curve
  !segend(maxmm)  Last time bin of each segment
  !t(nn)          Time at start of each bin
  implicit none
  character (len=500) infile
  integer nn,nseg,mm,maxmm,segend(maxmm)
  real dt,t(nn)
  integer status,unit,readwrite,blocksize,nn1,nhdu,gtihdu
  character (len=500) comment,exname
  real dt1
  logical exact,anynull
  integer startcol,stopcol,nGTI,i,hdutype,k,tcol,m
  double precision, allocatable :: tstart(:),tstop(:)
  double precision TIME,T0,tdoub(nn)
  !-----------------------------------------------------------------
  !Find a unit number not currently being used
  status = 0
  call ftgiou(unit,status)
  if( status .ne. 0 ) stop 'cannot open unused unit number'
  !-----------------------------------------------------------------
  !Open the light curve fits file with read-only access
  readwrite = 0
  status    = 0
  !write(*,*)trim(infile)
  call ftopen(unit,infile,readwrite,blocksize,status)
  if( status .ne. 0 ) stop 'cannot open light curve file'
  !-----------------------------------------------------------------
  !Shift to extension "RATE"
  status = 0
  call FTMNHD(unit, 2, 'RATE', 0, status)
  if(status .ne. 0) stop 'Cannot shift to RATE extension'
  !-----------------------------------------------------------------
  !Check number of rows in the table: nn1
  status = 0
  call ftgkyj(unit,'NAXIS2',nn1,comment,status)
  if(status .ne. 0) stop 'Cannot determine nn'
  if(nn.ne.nn1) stop 'nn changed!!!!'
  !-----------------------------------------------------------------
  !Check the time interval: dt1
  status = 0
  call ftgkye(unit,'TIMEDEL',dt1,comment,status)
  if(status .ne. 0) stop 'Cannot determine dt'
  if( abs(dt-dt1) .gt. 1e-5 ) stop 'dt changed!!!!'
  !-----------------------------------------------------------------
  !Get the number of the current extension to work out number of GTI extension
  call FTGHDN(unit,nhdu)
  if( nhdu .eq. 2 )then
     gtihdu = 3
  else if( nhdu .eq. 3 )then
     gtihdu = 2
  else
     stop 'dont know where to look for GTIs!'
  end if
  !-----------------------------------------------------------------
  !Go to GTI extension
  status = 0
  call FTMAHD(unit,gtihdu,hdutype,status)
  if(status .ne. 0) stop 'Cannot move to GTI extension!'
  !Get the name of this extension
  status = 0
  call ftgkys(unit,'EXTNAME',EXNAME,comment,status)
  if(status .ne. 0) stop 'Cannot get extension name'
  !write(*,*)"GTI extension=",trim(EXNAME)
  !-----------------------------------------------------------------
  !Get number of GTIs
  status = 0
  call ftgkyj(unit,'NAXIS2',nGTI,comment,status)
  if(status .ne. 0) stop 'Cannot determine nGTI'
  !-----------------------------------------------------------------
  !Get the column numbers for start and stop
  exact=.false.
  call ftgcno(unit,exact,'START',startcol,status)
  call ftgcno(unit,exact,'STOP',stopcol,status)
  !-----------------------------------------------------------------
  !Read in GTI start and stop times
  !write(*,*)"----------------------------------------------"
  !write(*,*)"List of GTIs..."
  allocate( tstart(nGTI) )
  allocate( tstop(nGTI) )
  status = 0
  do i = 1,nGTI
    !Read in tstart
    anynull = .false.
    call ftgcvd(unit,startcol,i,1,1,-1.0,tstart(i),anynull,status)
    !Read in tstop
    call ftgcvd(unit,stopcol,i,1,1,-1.0,tstop(i),anynull,status)
    !write(*,*)tstart(i),tstop(i)
  end do
  !write(*,*)"----------------------------------------------"
  if( status .ne. 0 ) stop 'Couldnt read in GTIs properly!'
  !-----------------------------------------------------------------
  !Shift back to extension "RATE"
  status = 0
  call FTMNHD(unit, 2, 'RATE', 0, status)
  if(status .ne. 0) stop 'Cannot shift to RATE extension'
  !-----------------------------------------------------------------
  !Get the column number for time
  exact=.false.
  call ftgcno(unit,exact,'TIME',tcol,status)
  !-----------------------------------------------------------------
  !Read in time and rate      
  m  = 0 !The number of good time bins counted in this segment so far
  mm = 0 !The number of good segments counted so far
  i  = 1 !The number of the current Good Time Interval
  do k = 1,nn
    !---------------------------------------------------------------
    !Read in TIME
    anynull = .false.
    call ftgcvd(unit,tcol,k,1,1,-1.0,TIME,anynull,status)
    if( k .eq. 1 ) T0 = TIME
    tdoub(k) = TIME - T0
    t(k)     = REAL( TIME-T0 )
    !---------------------------------------------------------------
    !Check for drop-outs
    if( k .gt. 1 )then
     if( tdoub(k) - tdoub(k-1) .gt. 1.1*dt )then
        m = 0
      else
        m = m + 1
      end if
    end if
    !---------------------------------------------------------------
    !Check that we're in a GTI
    do while( TIME .gt. tstop(i)+0.1*dt .and. i .lt. nGTI )
      i = i + 1
    end do
    if(TIME.lt.tstart(i)-0.1*dt.or.TIME.gt.tstop(i)+0.1*dt)then
      m = 0
    end if
    !---------------------------------------------------------------
    !If we get a full segment, record where it ends
    if( m .eq. nseg )then
      mm = mm + 1
      segend(mm) = k
      m  = 0
    end if       
  end do
  if( anynull ) write(*,*)'Problem reading in light curve'
  !Close and free up unit
  call ftclos(unit,status)
  call ftfiou(unit, status)
  return
end subroutine segsort
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
subroutine getnnanddt(infile,nn,dt)
  !Reads length and time step of the light curve
  !Inputs:
  !infile         Name of fits file containing light curve (char 500)
  !Outputs:
  !nn             Number of time bins in light curve (real)
  !dt             Duration of each time bin (integer)
  implicit none
  character (len=500) infile
  integer nn
  real dt
  integer status,unit,readwrite,blocksize
  character (len=500) comment
  !-----------------------------------------------------------------
  !Find a unit number not currently being used
  status = 0
  call ftgiou(unit,status)
  if( status .ne. 0 ) stop 'cannot open unused unit number'
  !-----------------------------------------------------------------
  !Open the light curve fits file with read-only access
  readwrite = 0
  status    = 0
  !write(*,*)trim(infile)
  call ftopen(unit,infile,readwrite,blocksize,status)
  if( status .ne. 0 ) stop 'cannot open light curve file'
  !-----------------------------------------------------------------
  !Shift to extension "RATE"
  status = 0
  call FTMNHD(unit, 2, 'RATE', 0, status)
  if(status .ne. 0) stop 'Cannot shift to RATE extension'
  !-----------------------------------------------------------------
  !Get number of rows in the table: nn
  status = 0
  call ftgkyj(unit,'NAXIS2',nn,comment,status)
  if(status .ne. 0) stop 'Cannot determine nn'
  !-----------------------------------------------------------------
  !Get the time interval: dt
  status = 0
  call ftgkye(unit,'TIMEDEL',dt,comment,status)
  if(status .ne. 0) stop 'Cannot determine dt'
  !-----------------------------------------------------------------
  !Close and free up unit
  call ftclos(unit,status)
  call ftfiou(unit, status)
  return
end subroutine getnnanddt
!-----------------------------------------------------------------------



!-----------------------------------------------------------------------
      subroutine mkfourchar(i,A)
! c
! c input the integer i and this outputs a character A which has
! c a length of 4 with leading zeros if required
! c
      integer i
      character (LEN=4) A
      if( i .lt. 10 )then
        write(A,'(A3,I1)')'000',i
      else if( i .lt. 100 )then
        write(A,'(A2,I2)')'00',i
      else if( i .lt. 1000 )then
        write(A,'(A1,I3)')'0',i
      else if( i .lt. 10000 )then
        write(A,'(I4)')i
      else
        write(*,*)"Integer too long in mkfourchar!"
      end if
      RETURN
      END subroutine mkfourchar
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
      subroutine mkthreechar(i,A)
! c
! c input the integer i and this outputs a character A which has
! c a length of 3 with leading zeros if required
! c
      integer i
      character (LEN=3) A
      if( i .lt. 10 )then
        write(A,'(A2,I1)')'00',i
      else if( i .lt. 100 )then
        write(A,'(A1,I2)')'0',i
      else if( i .lt. 1000 )then
        write(A,'(I3)')i
      else
        write(*,*)"Integer too long in mkthreechar!"
      end if
      RETURN
      END subroutine mkthreechar
!-----------------------------------------------------------------------




!-----------------------------------------------------------------------
      subroutine adv_mypow(nseg,C,nnmax,dt,mm,segend,norm,Ndet,taud,P,dP,mu,wn)
! c INPUT:
! c nseg        Number of time bins in a segment
! c C(nnmax)    Time series to transform
! c nnmax       Maximum length of time series
! c dt          Time interval
! c mm          Number of segments
! c segend(mm)  Last time bin in each segment
! c norm        |norm|=1: fractional rms normalisation
! c             |norm|=2: absolute rms normalisation
! c             norm<0: subtract white noise; norm>0: don't subtract
! c Ndet        Number of detectors added together to make light curve (i.e. Number of PCUs)
! c taud        Dead time (sseconds)        
! c OUTPUT:
! c P(nseg/2)   Output power spectrum
! c dP(nseg/2)  Error on power spectrum
! c mu          Mean count rate
! c wn(nseg/2)  Dead time affected Poisson noise
      implicit none
      integer nseg,nnmax,mm,norm,segend(nnmax/nseg),Ndet
      real C(nnmax),dt,P(nseg/2),dP(nseg/2),taud
      real df,mu,Cseg(nseg),wn(nseg/2),per(nseg/2),pi
      integer i,j,m
      df = 1. / ( float(nseg) * dt )
      pi = acos(-1.0)
      !Calculate average (non white noise subtracted) power spectrum
      !in absolute rms normalisation
      P  = 0.0
      mu = 0.0
      do m = 1,mm
        do i = 1,nseg
          j       = i + segend(m) - nseg
          Cseg(i) = C(j)
          mu      = mu + Cseg(i)
        end do
        call nperiodogram(dt,nseg,Cseg,per)
        P = P + per
      end do
      P  = P / float(mm)
      dP = P / sqrt( float(mm) )
      mu = mu / float(nseg*mm)
      !Calculate dead time affected Poisson noise
      do j = 1,nseg/2
        wn(j) = 1.0 - 2.0 * mu * taud * ( 1.0 - 0.5*taud/dt )          
        wn(j) = wn(j)-real(nseg-2)/real(nseg)*mu/real(Ndet)*taud**2/dt*cos(2.0*pi*real(j)/real(nseg))
        wn(j) = wn(j) * 2.0 * mu
      end do
      !Subtract white noise
      if( norm .lt. 0 ) P = P - wn
      !Convert to fractional rms normalisation
      if( abs(norm) .eq. 1 )then
        P  = P  / mu**2.0
        dP = dP / mu**2.0
        wn = wn / mu**2.0
      end if
      RETURN
    END subroutine adv_mypow
!-----------------------------------------------------------------------







  
!-----------------------------------------------------------------------
      subroutine mypow(nseg,C,nnmax,dt,mm,segend,norm,P,dP,mu,wn)
! c INPUT:
! c nseg        Number of time bins in a segment
! c C(nnmax)    Time series to transform
! c nnmax       Maximum length of time series
! c dt          Time interval
! c mm          Number of segments
! c segend(mm)  Last time bin in each segment
! c norm        |norm|=1: fractional rms normalisation
! c             |norm|=2: absolute rms normalisation
! c             norm<0: subtract white noise; norm>0: don't subtract
! c OUTPUT:
! c P(nseg/2)   Output power spectrum
! c dP(nseg/2)  Error on power spectrum
! c mu          Mean count rate
! c wn          White noise level
      implicit none
      integer nseg,nnmax,mm,norm,segend(nnmax/nseg)
      real C(nnmax),dt,P(nseg/2),dP(nseg/2)
      real df,mu,Cseg(nseg),wn,per(nseg/2),f
      integer i,j,m
      df = 1. / ( float(nseg) * dt )
      !Calculate average (non white noise subtracted) power spectrum
      !in absolute rms normalisation
      P  = 0.0
      mu = 0.0
      do m = 1,mm
        do i = 1,nseg
          j       = i + segend(m) - nseg
          Cseg(i) = C(j)
          mu      = mu + Cseg(i)
        end do
        call nperiodogram(dt,nseg,Cseg,per)
        P = P + per
      end do
      mu = mu / float(nseg*mm)
      wn = 2. * mu
      P  = P / float(mm)
      dP = P / sqrt( float(mm) )
      !Subtract white noise
      if( norm .lt. 0 ) P = P - wn
      !Convert to fractional rms normalisation
      if( abs(norm) .eq. 1 )then
        P  = P  / mu**2.0
        dP = dP / mu**2.0
        wn = wn / mu**2.0
      end if
      RETURN
    END subroutine mypow
!-----------------------------------------------------------------------




!------------------------------------------------------------------------
      subroutine ncperiodogram(dt,n,ht,st,rc,ic)
! c Calculates a cross spectrum between the time series ht(n) and st(n)
! c In absolute rms normalisation
! c Phase is such that +ve lag corresponds to ht lagging st
! c From Press et al (1992) DFT definition, this is S^*(\nu)H^*(\nu)
      implicit none
      integer n,j
      real ht(n),st(n),dt
      real datah(2*n),datas(2*n),rc(n/2),ic(n/2)
      do j = 1,n         
        datah(2*j-1) = ht(j)
        datah(2*j)   = 0.0
        datas(2*j-1) = st(j)
        datas(2*j)   = 0.0
      end do
      call four1(datah,n,1)
      call four1(datas,n,1)
      do j = 1, n/2
        rc(j) = datah(2*j+1)*datas(2*j+1)
        rc(j) = rc(j) + datah(2*j+2)*datas(2*j+2)
        rc(j) = rc(j) * 2. * dt / float(n)
        ic(j) = datah(2*j+1)*datas(2*j+2)
        ic(j) = ic(j) - datah(2*j+2)*datas(2*j+1)
        ic(j) = ic(j) * 2. * dt / float(n)
      end do
      return
    end subroutine ncperiodogram
!------------------------------------------------------------------------




!------------------------------------------------------------------------
      subroutine nperiodogram(dt,n,at,per)
! c Calculates the periodogram of the time series at(n) in absolute rms
! c normalisation (uses four1 from Press et al 1992)
      implicit none
      integer n,j
      real at(n),per(n/2)
      real data(2*n),dt
      do j = 1,n
        data(2*j-1) = at(j)
        data(2*j)   = 0.0
      end do
      call four1(data,n,1)
      do j = 1, n/2
        per(j) = data(2*j+1)**2.0 + data(2*j+2)**2.0
        per(j) = per(j) * 2. * dt / float(n)
      end do
      return
    end subroutine nperiodogram
!------------------------------------------------------------------------


!-----------------------------------------------------------------------
      SUBROUTINE four1(data,nn,isign)
      INTEGER isign,nn
      REAL data(2*nn)
      INTEGER i,istep,j,m,mmax,n
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      n=2*nn
      j=1
      do 11 i=1,n,2
        if(j.gt.i)then
          tempr=data(j)
          tempi=data(j+1)
          data(j)=data(i)
          data(j+1)=data(i+1)
          data(i)=tempr
          data(i+1)=tempi
        endif
        m=n/2
1       if ((m.ge.2).and.(j.gt.m)) then
          j=j-m
          m=m/2
        goto 1
        endif
        j=j+m
11    continue
      mmax=2
2     if (n.gt.mmax) then
        istep=2*mmax
        theta=6.28318530717959d0/(isign*mmax)
        wpr=-2.d0*sin(0.5d0*theta)**2
        wpi=sin(theta)
        wr=1.d0
        wi=0.d0
        do 13 m=1,mmax,2
          do 12 i=m,n,istep
            j=i+mmax
            tempr=sngl(wr)*data(j)-sngl(wi)*data(j+1)
            tempi=sngl(wr)*data(j+1)+sngl(wi)*data(j)
            data(j)=data(i)-tempr
            data(j+1)=data(i+1)-tempi
            data(i)=data(i)+tempr
            data(i+1)=data(i+1)+tempi
12        continue
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
13      continue
        mmax=istep
      goto 2
      endif
      return
      end
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
      subroutine mkonechar(i,A)
! c
! c input the integer i and this outputs a character A which has
! c a length of 1
! c
      integer i
      character (LEN=1) A
      if( i .lt. 10 )then
        write(A,'(I1)')i
      else
        write(*,*)"Integer too long in mkonechar!"
      end if
      RETURN
      END subroutine mkonechar
!-----------------------------------------------------------------------
